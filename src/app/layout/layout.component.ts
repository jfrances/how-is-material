import { Component, OnInit } from '@angular/core';

export interface Tile{
  color: string;
  cols: number;
  rows: number;
  text: string;
  text2: string;
  text3: string;
}

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})

export class LayoutComponent implements OnInit{
  tiles: Tile[] = [
    {text: 'One', cols: 3, rows: 1, color: 'lightblue', text2: 'One more', text3: 'One more time'},
    {text: 'Two', cols: 1, rows: 2, color: 'lightgreen', text2: 'Two more', text3: 'Two more time'},
    {text: 'Three', cols: 1, rows: 1, color: 'lightpink', text2: 'Three more', text3: 'Three more time'},
    {text: 'Four', cols: 2, rows: 1, color: '#DDBDF1', text2: 'Four more', text3: 'Four more time'}
  ];

  ngOnInit(){
  }
}
